function showFormCreate() {
    var masquer = document.getElementById('btn-newGame');
    masquer.remove();
    var form = document.createElement('form');
    form.method = 'post';
    form.enctype = 'multipart/form-data';
    form.innerHTML = '<label for="nom">Nom du jeu :</label> <input type="text" name="nom" id="nom"><br><label for="trancheAge">Tranche d\'âge :</label><input type="text" name="trancheAge" id="trancheAge"><br><label for="nbPoints">Nombre de points :</label><input type="text" name="nbPoints" id="nbPoints"><br><label for="imageFile">Image :</label><input type="file" name="imageFile" id="imageFile"><br><label for="description">Description :</label><textarea name="description" id="description"></textarea><br><input type="submit" value="Créer un jeu">';
    var button = document.createElement('button');
    button.onclick = hideFormCreate;
    button.id = 'hidebtn-newGame';
    button.innerHTML = 'Masquer le formulaire';
    document.querySelector('#formNewGame').appendChild(form);
    document.querySelector('#formNewGame').appendChild(button);
}

function hideFormCreate() {
    var container = document.getElementById('formNewGame');
    container.innerHTML = '';
    var button = document.createElement('button');
    button.textContent = 'Créer un jeu';
    button.id = 'btn-newGame';
    button.onclick = showFormCreate;
    container.appendChild(button);
}