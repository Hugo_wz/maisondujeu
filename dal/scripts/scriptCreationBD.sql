create table Jeu
(
    id SERIAL primary key not null,
    nom varchar(255),
    lienImage varchar(255),
    trancheAge varchar(255),
    nbPoints int
);

create table users
(
	id SERIAL primary key not null,
	username varchar(255),
	email varchar(255),
	password varchar(255)
);