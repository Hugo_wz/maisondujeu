<?php
include_once "bd.inc.php";

function createUser($username, $email, $passwd){
    $cnx = connexionBDD();
    $check_email_sql = "SELECT * FROM users WHERE email = $1";
    $check_email_result = pg_query_params($cnx, $check_email_sql, array($email));
    
    if (pg_num_rows($check_email_result) > 0) {
        echo "<p style=".'"'."color: red".'"'.">L'email est déjà utilisé. Veuillez choisir un autre email.</p>";
    }else{
    $hashed_password = password_hash($passwd, PASSWORD_DEFAULT);
    $sql = "INSERT INTO users (username, email, password) VALUES ($1, $2, $3)";
    $result = pg_query_params($cnx, $sql, array($username,$email,$hashed_password));}
}

function logUser($email, $passwd){
    $cnx = connexionBDD();
    $sql = "SELECT * FROM users WHERE email = $1";
    $result = pg_query_params($cnx, $sql, array($email));
    
    $row = pg_fetch_assoc($result);
    if ($row && password_verify($passwd, $row['password'])) {
        session_start();
        $_SESSION['logged_in'] = true;
        header("Location: index.php");
        exit;
    } else {
        echo "<p style=".'"'."color: red".'"'.">Identifiant ou mot de passe incorrects. Veuillez réessayer.</p>";
    }
}
