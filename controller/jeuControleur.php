<?php
/* Script du contrôleur responsable de la gestion des jeux */

global $root;
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/dal/bd.jeu.inc.php");

// Récupération de l'action définie dans l'URL
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "all";
}

// Gestion des différentes fonctionalités du contrôleur des jeux
switch($action) {
    case 'all':
        // Affichage de la liste des jeux

        // 1 - Récupération de la liste à partir de la BDD
        $jeux = getJeux();

        // 2 - Affichage de la vue gérant la liste
        include "$root/view/jeu/allJeu.html.php";
        break;

    case 'allespaces':
        $espaces = getEspaces();

        include "$root/view/jeu/allespaces.html.php";
        break;
    
    
    
        case 'item1':
        // appel des fonctions permettant de récupérer les donnees utiles à l'affichage
        // affichage de la vue
        include "$root/view/jeu/detail.html.php";
        break;

    default:
        include "$root/view/error/400.html.php";
}
