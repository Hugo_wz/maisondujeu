<?php
/* Script du contrôleur principal du site */

global $root;
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/dal/bd.accueil.php");

// recuperation de l'action définie dans l'URL
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "presentation";
}

// Gestion des différentes fonctionalités du contrôleur du site
switch($action) {
    case 'presentation':
        // appel des fonctions permettant de récupérer les données utiles à l'affichage
        // affichage de la vue
        $ho = getHoraire();
        $event = getEvenement();
        include "$root/view/site/sitePresentation.html.php";
        break;

    case 'item1':
        // appel des fonctions permettant de récupérer les données utiles à l'affichage
        // affichage de la vue
        include "$root/view/site/pageEnCoursConstruction.html.php";
        break;

    default:
        include "$root/view/error/400.html.php";
}

