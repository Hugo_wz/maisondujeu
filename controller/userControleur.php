<?php
/* Script du contrôleur responsable de la gestion des jeux */

global $root;
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

// Récupération de l'action définie dans l'URL
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "reg";
}


switch($action) {
    case 'reg':
        include "$root/view/user/register.html.php";
        break;

    case 'log':
        include "$root/view/user/login.html.php";
        break;

    default:
        include "$root/view/error/400.html.php";
}
