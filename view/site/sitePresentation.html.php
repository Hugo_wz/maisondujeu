<?php
global $root, $ho, $event; 
include "$root/view/header.html.php";


?>
    <h1 class="title">Bienvenue à la MaisonDuJeu</h1>
    <img src="./view/img/mdjimg.png" alt="logo maison du jeu" width="350" height="350"><br>
    <a href="./index.php?object=user&action=log">Connexion</a><br>
    <img width="500px" height="auto" src="./view/img/plan.png" alt="plan de localisation de la maison du jeu">  
    <!-- Ajout du bouton de connexion -->

   
<?php





echo "<ul>";
foreach ($ho as $key => $value) {
    echo "<li>";
    foreach ($value as $heure => $tqt) {
        echo $tqt;
    }
    echo "</li>";
}
?>

<div class="container">
    <div class="tarifs">
    <h3>Tarif individuel : </h3>
    <pre>
        20€/an
        tarif réduit : 10€/an (étudiants, sans-emploi, retraité(e))
    </pre>

    <h3>Adhésion familliale : </h3>
    <pre>
        40€/an si il y a 2 salaires au sein du foyer
        30€/an si il y a 1 salaires au sein du foyer
        20€/an si il n'y a pas de salaires au sein du foyer
        Une adhésion familliale prend en compte 2 adultes et les enfants du foyer
        
    </pre>

    <h3>Adhésion assistant(e) maternel(le) </h3>
    <pre>
        30€/an
        Cette adhésion permet à un(e) assistante maternel(le) de venir gratuitement, accompagné(e) de 5 enfants maximum.
    </pre>
    </div>
</div>


<?php   
echo "</ul>";
include "$root/view/footer.html.php";
?>

