<?php
global $root, $reg;
include "$root/view/header.html.php";
include "$root/dal/bd.user.php"
?>

<h1>
    Créez votre compte
</h1>

<form method="post">
    <label for="username">Nom d'utilisateur :</label><br>
    <textarea name="username" id="username" cols="25" rows="1" required></textarea><br><br>
    <label for="email">Adresse email :</label><br>
    <textarea name="email" id="email" cols="25" rows="1" required></textarea><br><br>
    <label for="passwd">Mot de passe :</label><br>
    <textarea name="passwd" id="passwd" cols="25" rows="1" required></textarea><br><br>
    <input type="submit" value="Créer le compte">
</form>

<a href="./index.php?object=user&action=log">Vous avez deja un compte? Connectez-vous!</a>



<?php 
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $username = $_POST['username'];
    $email = $_POST['email'];
    $passwd = $_POST['passwd'];
    if (!empty($email) && preg_match('/^.+@.+\..+$/', $email)) {
        createUser($username, $email, $passwd);
    } else {
        echo "<p style=".'"'."color: red".'"'.">L'email n'est pas dans un format valide.</p>";
    }
}

include "$root/view/footer.html.php";
?>
