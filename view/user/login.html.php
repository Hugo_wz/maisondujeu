<?php
global $root, $reg;
include "$root/view/header.html.php";
include "$root/dal/bd.user.php"
?>

<h1>Connectez vous à votre compte</h1>

<form method="post">
    <label for="email">Adresse email :</label><br>
    <textarea name="email" id="email" cols="25" rows="1" required></textarea><br><br>
    <label for="passwd">Mot de passe :</label><br>
    <textarea name="passwd" id="passwd" cols="25" rows="1" required></textarea><br><br>
    <input type="submit" value="connexion">
</form>

<?php 
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $email = $_POST['email'];
    $passwd = $_POST['passwd'];
    logUser($email, $passwd);
}

include "$root/view/footer.html.php";
?>
