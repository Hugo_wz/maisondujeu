<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <?php $title = " Welcome to Hackat'Web"; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title><?php echo $title ?></title>
    <style type="text/css">
        @import url("public/css/base.css");
        @import url("public/css/corps.css");
    </style>
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.ico">
</head>
<body>
    <nav>
        <ul id="menuGeneral">
            <li><a href="./index.php?object=site&action=presentation">Accueil</a></li>
            <li><a href="./index.php?object=jeu&action=all">Jeux</a></li>
            <li><a href="./index.php?object=jeu&action=allespaces">Espaces</a></li>
            <?php
            if (isset($_SESSION['logged_in'])) {
                echo '<li><a href="./controller/logout.php">Déconnexion</a></li>';
            } else {
                echo '<li><a href="./index.php?object=user&action=reg">Register</a></li>';
                echo '<li><a href="./index.php?object=user&action=log">Login</a></li>';
            }
            ?>   
                     
        </ul>
    </nav>
    <div id="corps">