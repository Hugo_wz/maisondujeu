<?php
global $root, $espaces;
include "$root/view/header.html.php";
?>

<h1>
    Espaces de jeu de rôle disponibles à la location
</h1>

<?php
foreach ($espaces as $espace) {
    print($espace["nom"]. "<br>");
    print($espace["description"]."<br>");
    print($espace["spec"]."<br>");
    print($espace["tarif"]."<p> </p><br>");
};

include "$root/view/footer.html.php";
?>