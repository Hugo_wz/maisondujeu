<?php
global $root, $jeux;
include "$root/view/header.html.php";
?>

<h1>
    Jeux disponibles à la location
</h1>


<div id="formNewGame">
<button id="btn-newGame" onclick="showFormCreate()">Créer un jeu</button>
</div>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nom = $_POST['nom'];
    $trancheAge = $_POST['trancheAge'];
    $nbPoints = $_POST['nbPoints'];
    $target_dir = "$root/public/images/jeu/";
    $target_file = $target_dir . basename($_FILES["imageFile"]["name"]);
    if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
        echo "Le fichier " . htmlspecialchars(basename($_FILES["imageFile"]["name"])) . " a été téléchargé avec succès.";}
    $descriptionJeu = $_POST['description'];
    addJeu($nom, basename($_FILES["imageFile"]["name"]), $trancheAge, $nbPoints, $descriptionJeu);
}

    foreach ($jeux as $jeu) {
        print('<img src="public/images/jeu/'.$jeu["lienimage"].'" alt="description alt." width="100" height="50">');
        print($jeu["nom"]."<br>");
    };
include "$root/view/footer.html.php";?>

